# dahua-scanner

- может находить камеры Dahua DVR, NVR, IPC по указанным диапазонам через Masscan
- брутит каждую камеру парами логинов/паролей из `combinations.txt` либо перебором из файлов `logins.txt` и `passwords.txt`
- снимает скриншоты с каждой камеры и сохраняет в папку `snapshots`
- экспортит результаты в виде XML для программы SmartPSS
 
## Требования
- Python 3 - установить в систему, также нужен PIP
- модули Python: `pip install -r requirements.txt`
- Masscan - [собрать из исходников](https://github.com/robertdavidgraham/masscan) или скачать [готовый для Windows](https://mega.nz/#!R5tXlAZa!IihDrWMrCrXXjZz9nTkyBFDWi3OrClw70Bp_kSn9xv4) и распаковать masscan.exe в папку с программой (не забыть установить WinPcap из того же архива!)
- Smart PSS - [скачать](http://www.safemag.ru/smart-pss/l)

### Использование

- указать диапазоны IP в файле `scan.txt`
- указать комбинациии логинов и паролей в файле `combinations.txt`, по умолчанию брут идет по ним (в большинстве камер ограничение в 5 попыток входа)
- если надо будет брутить по логинам и паролям, то указать флаг `-l` и отредактировать файлы `logins.txt` и `passwords.txt`
- если надо запустить Masscan, то указываем аргументы `-m` и `-s`: `./dahua-scanner.py -m -s scan.txt`
- если надо просто пробрутить по файлу с IP, то указываем аргумент `-b`: `./dahua-scanner.py -b ips.txt`
- подождать окончания работы и загрузить *.xml в программу SmartPss