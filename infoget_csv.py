#!/usr/bin/python3
import requests
import csv
import optparse
import logging
import os.path
import concurrent.futures
from utils.dahua import DahuaController, HTTP_API_REQUESTS

logging.basicConfig(level=logging.INFO, format='[%(asctime)s] [%(levelname)s] %(message)s')
logging.getLogger("requests").setLevel(logging.WARNING)
logger = logging.getLogger('dahua')
logger.setLevel(logging.INFO)

options = optparse.OptionParser('%prog' + " -c file OR -i IP")
options.add_option('-c', dest='csv', type='string',
                  help='File with CSV camera info')
options.add_option('-i', dest='ip', type='string',
                  help='IP address of camera')
(args, _) = options.parse_args()

if not args.csv and not args.ip:
    logger.error("Specify CSV file with -c option or IP address with -i option")
    exit()

fieldnames = ['IP', 'Port', 'Login', 'Password', 'Channels']

login = 'user'
password = 'user'

all_fieldnames = sum([list(request['fields'].values()) for request in HTTP_API_REQUESTS], [])

results = []
total_cams = 0

if args.ip:
    res = get_extended_info({'IP': args.ip, 'Login': login, 'Password': password})
    print(res)

else:
    logger.info('Try to get extended info from cameras HTTP API...')
    cams_reader = csv.DictReader(open(args.csv, newline=''), delimiter=',', fieldnames=fieldnames)
    cams_writer = None

    def get_ex_info(cam):
        global total_cams, results
        total_cams += 1
        data = DahuaController.get_extended_info(cam)
        if data:
            results.append(data)
        return data

    try:
        with concurrent.futures.ThreadPoolExecutor(max_workers=150) as pool:
            pool.map(get_ex_info, cams_reader)
    except:
        logger.info('Extended info getting process interrupt!')

    if results:
        logger.info('Results: %d devices bruted, %d HTTP API found' % (total_cams, len(results)))
        fieldnames = fieldnames + ['http_port'] + all_fieldnames
        filename = os.path.join(os.path.dirname(args.csv), 'extended_' + os.path.basename(args.csv))
        cams_writer = csv.DictWriter(open(filename, 'w', newline=''), delimiter=',', fieldnames=fieldnames)
        cams_writer.writeheader()

        for result in results:
            cams_writer.writerow(result)

        logger.info('Extended report saved to {}'.format(filename))
